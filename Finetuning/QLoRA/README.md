# Brief description of content in each file

## Llama2_7B.ipynb

Purpose: Fine-tune Meta's Llama2-7B base model

Type of training: Instruction fine-tuning

Method used: PEFT-QLoRA

Dataset used: https://huggingface.co/datasets/databricks/databricks-dolly-15k

Number of labelled examples used (from the dataset): 15011

Type of quantization used: 4-bit

<!-- Number of trainable parameters: 160 -->

Compute used (AWS): g5.4xlarge (EC2) instance i.e.
- 1 NVIDIA A10G GPU (24 GB RAM)
- 16 CPUs; 64 GB (total)

Time taken (to execute): ~ 5.5 hours

Cost incurred: 11.165 USD
i.e. 2.03 USD (per hour) * 5.5 

Reference to original notebook: https://github.com/philschmid/sagemaker-huggingface-llama-2-samples/blob/master/training/sagemaker-notebook.ipynb

## scripts\run_clm.py
- implements QLoRA to train our model
- merges the LoRA weights into the model weights after training. 
i.e. We can use the model as a normal model without any additional code.
